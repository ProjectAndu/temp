﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Rig
{
    BODY,
    LEGS
};

public class AnyStateAnimation
{
    public Rig AnimationRig { get; private set; }
    
    public string Name { get; set;}
    
    public bool Active { get; set; }

    public string[] HigherPriority { get; set; }
    
    public AnyStateAnimation(Rig rig, string name, params string[] higherPriority) {
        this.AnimationRig = rig;
        this.Name = name;
        this.HigherPriority = higherPriority;
    }
    
    
    
    

}

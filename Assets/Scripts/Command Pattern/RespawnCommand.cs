﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RespawnCommand : Command
{
    
    private Player player;
    public RespawnCommand(Player player,KeyCode key) : base(key) {
        this.player = player;
    }

    public override void GetKeyDown() {
        player.Utilities.Die();


    }
}
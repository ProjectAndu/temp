﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlideCommand: Command
{  
    private Player player;
    
    
    public SlideCommand(Player player,KeyCode key) : base(key) {
        this.player = player;
    }

    public override void GetKeyDown() {
        player.Actions.Slide();

    }

}

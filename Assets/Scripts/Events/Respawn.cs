﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Respawn : MonoBehaviour
{

    [SerializeField] private Player player;
    [SerializeField] private Transform respawnPoint;


    private void OnTriggerEnter2D(Collider2D other) {
        player.transform.position = respawnPoint.transform.position;
        player.Utilities.takeDamage(10);
    }
}

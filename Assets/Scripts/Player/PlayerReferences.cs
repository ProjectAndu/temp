﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PlayerReferences
{
    [SerializeField] private GameObject[] weaponsObjects;

    public GameObject[] WeaponsObjects
    {
        get => weaponsObjects;
        set => weaponsObjects = value;
    }
}

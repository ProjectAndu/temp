﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum AnimationsNames
{
    Body_Walk,
    Body_Idle,
    Body_Attack,
    Body_Slide,
    Legs_Walk,
    Legs_Idle,
    Legs_Attack,
    Legs_Slide

}

public class Player : MonoBehaviour
{
    [SerializeField] private PlayerStats stats;
    
    [SerializeField] private PlayerComponents components;
    
    [SerializeField] private PlayerReferences references;
    
    private PlayerUtilities utilities;
    
    private PlayerActions actions;

    public PlayerComponents Components
    {
        get => components;
    }

    public PlayerStats Stats => stats;

    public PlayerActions Actions => actions;

    public PlayerUtilities Utilities => utilities;

    public PlayerReferences References => references;

    public HealthBar healthBar;
    
    





    private void Awake() {
    }


    void Start()
    {
        actions = new PlayerActions(this);
        utilities = new PlayerUtilities(this);
        stats.speed = stats.WalkSpeed;
        AnyStateAnimation[] animations = new AnyStateAnimation[]{
            new AnyStateAnimation(Rig.BODY, AnimationsNames.Body_Idle.ToString(),AnimationsNames.Body_Attack.ToString(), AnimationsNames.Body_Slide.ToString()),
            new AnyStateAnimation(Rig.BODY, AnimationsNames.Body_Walk.ToString(), AnimationsNames.Body_Attack.ToString()),
            new AnyStateAnimation(Rig.BODY, AnimationsNames.Body_Attack.ToString()),
            new AnyStateAnimation(Rig.BODY, AnimationsNames.Body_Slide.ToString()),
            
            new AnyStateAnimation(Rig.LEGS, AnimationsNames.Legs_Idle.ToString(), AnimationsNames.Legs_Attack.ToString(), AnimationsNames.Legs_Slide.ToString()),
            new AnyStateAnimation(Rig.LEGS, AnimationsNames.Legs_Walk.ToString()),
            new AnyStateAnimation(Rig.LEGS, AnimationsNames.Legs_Attack.ToString()),
            new AnyStateAnimation(Rig.LEGS, AnimationsNames.Legs_Slide.ToString())
            

        };
        components.Animator.AddAnimations(animations);
        
        healthBar.SetMaxHealth(stats.MAXHealth);
        stats.CurrentHealth = stats.MAXHealth;

        components.DeathCounterText = GameObject.Find("Death").GetComponent<Text>();
        components.DeathCounterText.text = "Death Count: " + stats.deathCounter;


    }

    void Update()
    {
        Utilities.HandleInput();
        
    }

    private void FixedUpdate()
    {
        Actions.Move(transform);
    }
}

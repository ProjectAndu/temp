﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PlayerStats
{
    
    
    [SerializeField] private float walkSpeed;

    [SerializeField] private float runSpeed;
    
    [SerializeField] private float jumpForce;
    
    [SerializeField] private float slideSpeed;

    private bool canDoubleJump;

    public int deathCounter = 0;

    private WEAPON weapon;
    [SerializeField] private int maxHealth;
    [SerializeField] private int currentHealth;
    
    public float speed { get; set; }
    public Vector2 direction { get; set; }
    
    

    public float WalkSpeed => walkSpeed;

    public float RunSpeed => runSpeed;

    public float JumpForce => jumpForce;



    public bool CanDoubleJump
    {
        get => canDoubleJump;
        set => canDoubleJump = value;
    }

    public WEAPON Weapon
    {
        get => weapon;
        set => weapon = value;
    }

    public int MAXHealth => maxHealth;

    public int CurrentHealth
    {
        get => currentHealth;
        set => currentHealth = value;
    }

    public float SlideSpeed => slideSpeed;
}

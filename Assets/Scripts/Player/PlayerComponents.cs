﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class PlayerComponents
{
    [SerializeField] private Rigidbody2D rigidBody;

    [SerializeField] private LayerMask groundLayer;
    
    [SerializeField] private AnyStateAnimator animator;

    [SerializeField] private Collider2D collider;
    
    [SerializeField] private Collider2D slideCollider;
    public Rigidbody2D RigidBody => rigidBody;

    public AnyStateAnimator Animator
    {
        get => animator;
        set => animator = value;
    }

    public LayerMask GroundLayer => groundLayer;

    public Collider2D Collider => collider;

    public Collider2D SlideCollider => slideCollider;

    public Text DeathCounterText
    {
        get => deathCounterText;
        set => deathCounterText = value;
    }

    private Text deathCounterText;

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerUtilities
{
    private Player player;
    private List<Command> commands = new List<Command>();
    
    public PlayerUtilities(Player player) {
        this.player = player;
        commands.Add(new JumpCommand(player, KeyCode.Space));
        commands.Add(new AttackCommand(player, KeyCode.LeftControl));
        commands.Add(new WeaponSwapCommand(player, WEAPON.FISTS, KeyCode.Alpha1));
        commands.Add(new WeaponSwapCommand(player, WEAPON.SWORD, KeyCode.Alpha2));
        commands.Add(new WeaponSwapCommand(player, WEAPON.GUN, KeyCode.Alpha3));
        commands.Add(new RespawnCommand(player, KeyCode.X));
        commands.Add(new SlideCommand(player, KeyCode.LeftShift));
        

    }
    
    
    // ReSharper disable Unity.PerformanceAnalysis
    public void HandleInput() {

         player.Stats.direction =new Vector2(Input.GetAxisRaw("Horizontal"), player.Components.RigidBody.velocity.y);
         foreach (Command command in commands){
             if (Input.GetKeyDown(command.Key)){
                 command.GetKeyDown();
             }
             if (Input.GetKeyUp(command.Key)){
                 command.GetKeyUp();
             }
             if (Input.GetKey(command.Key)){
                 command.GetKey();
             }
         }
         

    }

    public bool isGrounded() {
        RaycastHit2D hit = Physics2D.BoxCast(player.Components.Collider.bounds.center, 
                                            player.Components.Collider.bounds.size,
                                            0, Vector2.down, 0.1f,
                                            player.Components.GroundLayer);
        return hit.collider != null;
    }

    public void takeDamage(int damage) {
        player.Stats.CurrentHealth -= damage;
        player.healthBar.SetHealth(player.Stats.CurrentHealth);
        if (player.Stats.CurrentHealth <= 0){
            Die();
        }
    }
    
    public void Die() {
        player.Stats.deathCounter++;
        player.Components.DeathCounterText.text = "Death Count: " + player.Stats.deathCounter;
        
        

    }

}

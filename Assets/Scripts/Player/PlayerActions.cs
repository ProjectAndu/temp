﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerActions
{
    private Player player;

    public PlayerActions(Player player) {
        this.player = player;
    }

    public void Move(Transform transform)
    {
        player.Components.RigidBody.velocity = new Vector2(player.Stats.direction.x * player.Stats.speed * Time.deltaTime,
                                                            player.Components.RigidBody.velocity.y);
        if (player.Stats.direction.x != 0){
            transform.localScale = new Vector3(player.Stats.direction.x < 0 ? -1 : 1, 1, 1);
            player.Components.Animator.TryPlayAnimation("Body_Walk");
            player.Components.Animator.TryPlayAnimation("Legs_Walk");
            
        }
        else if(player.Components.RigidBody.velocity == Vector2.zero){
            player.Components.Animator.TryPlayAnimation("Body_Idle");
            player.Components.Animator.TryPlayAnimation("Legs_Idle");
        }

    }

    public void Jump() {
        
        if(player.Utilities.isGrounded()){
            player.Stats.CanDoubleJump = true;
            doJump();
        }else if (player.Stats.CanDoubleJump){
            player.Stats.CanDoubleJump = false;
            doJump();
        }


        void doJump() {
            player.Components.RigidBody.velocity = Vector2.up * player.Stats.JumpForce;
            // player.Components.RigidBody.AddForce(new Vector2(0, player.Stats.JumpForce), ForceMode2D.Impulse);
            
        }
    }

    public void Attack() {
        player.Components.Animator.TryPlayAnimation("Body_Attack");
        player.Components.Animator.TryPlayAnimation("Legs_Attack");
        
    }

    public void TrySwapWeapon(WEAPON weapon) {
        player.Stats.Weapon = weapon;
        SwapWeapon();
    }

    public void SwapWeapon() {
        for (int i = 1; i < player.References.WeaponsObjects.Length; i++){
            player.References.WeaponsObjects[i].SetActive(false);
            
        }

        if (player.Stats.Weapon > 0){
            player.References.WeaponsObjects[(int)player.Stats.Weapon].SetActive(true);
        }

    }
    
    public void Slide() {
        player.Components.Animator.TryPlayAnimation("Body_Slide");
        player.Components.Animator.TryPlayAnimation("Legs_Slide");

        player.Components.Collider.enabled = false;
        player.Components.SlideCollider.enabled = true;
        
        if (player.Stats.direction.x < 0){
            player.Components.RigidBody.AddForce(Vector2.right * player.Stats.SlideSpeed);
        }else if (player.Stats.direction.x > 0){
            player.Components.RigidBody.AddForce(Vector2.left * player.Stats.SlideSpeed);

        }

    }


}
